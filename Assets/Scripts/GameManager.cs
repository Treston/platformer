﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager game;
    public Image fadeImage;
    public float fadeDuration = 1;

    void Awake () 
    {
        AudioManager.instance.Play("Dungeon Music");

        if (game == null) 
        {
            game = this;
            DontDestroyOnLoad(gameObject);
        }
        else 
        {
            Destroy(gameObject);
        }
    }

    public static void LoadScene (string scene, int doorID)
    {
        game.StartCoroutine(game.LoadSceneCoroutine(scene, doorID));
    }

    IEnumerator LoadSceneCoroutine (string scene, int doorID)
    {
        Time.timeScale = 0;

        fadeImage.enabled = true;
        Color startColor = fadeImage.color;
        startColor.a = 0;
        Color endColor = startColor;
        endColor.a = 1;
        for (float t = 0; t < fadeDuration; t += Time.unscaledDeltaTime)
        {
            float frac = t / fadeDuration;
            fadeImage.color = Color.Lerp(startColor, endColor, frac);
            yield return new WaitForEndOfFrame();
        }

        yield return SceneManager.LoadSceneAsync(scene);
        DoorController[] doors = GameObject.FindObjectsOfType<DoorController>();
        foreach (DoorController door in doors)
        {
            if (doorID == door.destinationID)
            {
                PlayerCOntroller.player.transform.position = door.transform.position;
                break;
            }
        }
         
        for (float t = 0; t < fadeDuration; t += Time.unscaledDeltaTime)
        {
            float frac = t / fadeDuration;
            fadeImage.color = Color.Lerp(endColor, startColor, frac);
            yield return new WaitForEndOfFrame();
        }
        fadeImage.enabled = false;

        Time.timeScale = 1;
    }

    
  /*public static void LoadSaveFile (SavePointController.SaveState save)
    {
        game.StartCoroutine(game.LoadSceneAsync(saveState.scene));
    }

     IEnumerator LoadSaveFileCoroutine (SavePointController.SaveState save)
    {
        yield return SceneManager.LoadSceneAsync(saveState.scene);
        PlayerCOntroller.player.transform.position = saveState.position;
    }*/
}
