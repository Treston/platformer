﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraController : MonoBehaviour
{
    public float speed = 0.25f;
    public Transform target;
    public Vector3 offset = new Vector3(2, 0, 0);

    void FixedUpdate ()
    {
        if (target == null) return;

        if (target.transform.rotation.y == 0)
        {
            transform.position = Vector3.Lerp(transform.position, target.position + offset, Time.fixedDeltaTime * speed);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, target.position + new Vector3 (-offset.x, offset.y, offset.z), Time.fixedDeltaTime * speed);
        }
    }
#if UNITY_EDITOR
    private void Update()
    {
        if (!Application.isPlaying && target != null)
        {
            transform.position = target.position + offset;
        }
    }
#endif
}
