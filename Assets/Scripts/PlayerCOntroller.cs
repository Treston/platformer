﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCOntroller : MonoBehaviour
{
    public static PlayerCOntroller player;

    public LayerMask interactablesLayer = -1;
    
    CharacterMotor motor;

    void Awake ()
    {
        if (player == null) 
        {
        player = this;
        DontDestroyOnLoad(gameObject);
        }

        else 
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        motor = GetComponent<CharacterMotor>();
    }

    void Update()
    {
        motor.xInput = Input.GetAxis("Horizontal");
        motor.shouldJump = Input.GetButtonDown("Jump");
        motor.cancelJump = Input.GetButtonUp("Jump");
        if (Input.GetButtonDown("Fire1"))
        {
            Interact();
        }
    }

    void Interact()
    {
        Collider2D collider = Physics2D.OverlapCircle(transform.position, 0.25f, interactablesLayer);
        if (collider == null) return;

        InteractableController interactable = collider.gameObject.GetComponent<InteractableController>();
        if (interactable == null) return;

        interactable.Interact();
    }
}