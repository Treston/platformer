﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{
    public delegate void YesNoCallback (bool answer);

    public static TextController instance;
    public GameObject textbox;
    public GameObject yesNoBox;
    public Transform yesNoSelector;
    public Transform yesOption;
    public Transform noOption;
    public GameObject icon;
    public float delay = 0.1f;
    public Text text;

    Queue<IEnumerator> textQueue = new Queue<IEnumerator>();

    void Awake()
    {
        if (instance == this) instance = this;

        text = textbox.GetComponentInChildren<Text>();
        
    }    

    void Setup ()
    {
        textbox.SetActive(true);
        Time.timeScale = 0;
    }

    void Cleanup ()
    {
        textbox.SetActive(false);
        Time.timeScale = 1;
        icon.SetActive(false);
    }

    IEnumerator Start ()
    {
        textbox.SetActive(false);
        while (enabled)
        {
            if (textQueue.Count > 0)
            {
                Setup();
                while(textQueue.Count > 0)
                {
                    yield return StartCoroutine(textQueue.Dequeue());
                }
                Cleanup();
            }
            yield return new WaitForEndOfFrame();
        }
    }
/* 
    public static void AskYesNoAndWait (string question, YesNoCallback CallBack)
    {
        instance.textQueue.Enqueue(instance.AskYesNoAndWaitCoroutine(question));
    }

    IEnumerator AskYesNoAndWaitCoroutine (string question, YesNoCallback CallBack)
    {
        yield return StartCoroutine(RevealTextCoroutine(question));
        yesNoBox.SetActive(true);
        yesNoSelector.position = yesOption.position;
        bool choice = true;
        while (!submitPressed)
        {
            if (choice && x > 0)
            {
                yesNoSelector.position = noOption;
                choice = false;
            }
            else if (!choice && x < 0)
            {
                yesNoSelector.position = yesOption.position;
                choice = true;
            }
            yield return new WaitForEndOfFrame();
        }
        CallBack(choice);
    }
*/
    public static void DisplayTextAndWait (string text)
    {
        instance.textQueue.Enqueue(instance.DisplayTextAndWaitCoroutine(text));
    }

    IEnumerator DisplayTextAndWaitCoroutine (string text)
    {
        this.text.text = text;
        yield return StartCoroutine(FlashIconAndWait());
    }

    public static void RevealText (string text)
    {
        instance.textQueue.Enqueue(instance.RevealTextCoroutine(text));
    }

    IEnumerator RevealTextCoroutine (string text)
    {
        this.text.text = "";
        for (int i = 0; i < text.Length; i++)
        {
            this.text.text = text.Substring(0, i);
            if (submitPressed) yield return new WaitForEndOfFrame();
            yield return new WaitForSecondsRealtime(delay);
        }
        yield return StartCoroutine(FlashIconAndWait());
    }

    IEnumerator FlashIconAndWait ()
    {
        while (submitPressed)
        {
            icon.SetActive(!icon.activeSelf);
            for (float t = 0; t < 0.5f; t += Time.unscaledDeltaTime)
            {
                if (submitPressed) break;
                yield return new WaitForEndOfFrame();
            }
            
        }
    }

    bool submitPressed;
    void Update ()
    {
        submitPressed = Input.GetButton("Submit");
    }
}
