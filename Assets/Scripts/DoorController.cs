﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : InteractableController
{
    public string scene;
    public int doorID = 0;
    public int destinationID = 0;

    public override void Interact()
    {
        GameManager.LoadScene(scene, destinationID);
    }
}
