﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMotor : MonoBehaviour
{
    public float speed = 3;
    public float maxSpeedChange = 2;
    public float minJumpHeight = 0.5f;
    public float jumpHeight = 5;
    public float jumpWindow = 0.25f;
    public float groundDist = 0.1f;
    public float jumpGravity = 5;
    public float fallGravity = 0.5f;

    public LayerMask envLayers = -1;
    Rigidbody2D body;
    Animator anim;
    Vector2 targetVelocity;

    bool onGround = false;
    float lastGrounded = -1;
    float _xInput = 0;
    bool _shouldJump = false;
    bool _cancelJump = false;

    public float xInput
    {
        get { return _xInput; }
        set { _xInput = value; }
    }

    public bool shouldJump
    {
        get { return _shouldJump; }
        set { _shouldJump = value; }
    }

    public bool cancelJump
    {
        get { return _cancelJump; }
        set { _cancelJump = value; }
    }  

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        AttackTest ();
        GroundTest ();
        //TeeterTest ();
        targetVelocity = body.velocity;
        targetVelocity.x = xInput * speed;
        anim.SetFloat("speed", Mathf.Abs(targetVelocity.x));

        if (xInput > 0.1f)
        {
            transform.localEulerAngles = Vector3.zero; 
        }

        else if (xInput < -0.1f)
        {
            transform.localEulerAngles = Vector3.up * 180f;    
        }

        if (lastGrounded > Time.time - jumpWindow && shouldJump)
        {
            lastGrounded -= jumpWindow;
        }

        if (Input.GetButtonDown("Jump"))
        {
            anim.SetTrigger("jump");
            Vector2 v = body.velocity;
            v.y = GetJumpSpeed();
            body.velocity = v;
            AudioManager.instance.Play("Jump");
        }
        
        targetVelocity.y = body.velocity.y;
        anim.SetFloat("yVel", targetVelocity.y);
    }
    
    void FixedUpdate ()
    {
        Vector2 velocityChange = targetVelocity - body.velocity;
        if (Mathf.Abs(velocityChange.x) > maxSpeedChange){
            velocityChange.x =maxSpeedChange * Mathf.Sign(velocityChange.x);
        }
        body.AddForce(velocityChange * body.mass / Time.fixedDeltaTime);
    }

    float GetJumpSpeed (float height)
    {
        return Mathf.Sqrt(2 * height * Mathf.Abs(Physics2D.gravity.y) * body.gravityScale);
    }

    float GetJumpSpeed ()
    {
        return GetJumpSpeed(jumpHeight);
    }

    void GroundTest ()
    {
       Collider2D[] env = Physics2D.OverlapCircleAll((Vector2)transform.position, groundDist, envLayers);
       onGround = env.Length > 0;
        if (onGround) {
            lastGrounded = Time.time;
            body.gravityScale = jumpGravity;
            AudioManager.instance.Play("Hit");
        }
        else if (body.velocity.y < 0)
        {
            body.gravityScale = fallGravity;
        }
        anim.SetBool("onGround", env.Length > 0);
    }

    /*
    void TeeterTest ()
    {
        Vector2 origin = transform.position + transform.right * 0.25f;
        Collider2D[] env = Physics2D.OverlapCircleAll(origin, 0.1f, envLayers);
        anim.SetBool("teetering", env.Length == 0);
    }
    */

    void AttackTest ()
    {
        
    }

    void OnDrawGizmos ()
    {
        Gizmos.DrawLine(transform.position, transform.position + Vector3.up * jumpHeight);
    }

    void Die()
    { 
        anim.SetTrigger("die");
        Vector3 dir = transform.position;
    }
}
