﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class SavePointController : MonoBehaviour
{
  [System.Serializable]
  public class SaveState 
  {
    public string scene;
    public Vector3 position;
  }
/*
  public string question = "Would you like to save the game";
 
  public override void Interact ()
  {
    TextController.AskYesNoAndWait(question, ResponedCallback);
  }

  void ResponseCallback (bool choice)
  {
    if (choice) Debug.Log("Yes Save");
    else Debug.Log("No No Save");
  }

  void Save ()
  {
    SaveState saveState = new SaveState();
    saveState.scene = SceneManager.GetActive().name;

    PlayerController player = PlayerController.player;
    saveState.position = player.transform.position;

    string path = Path.Combine(Application.persistentDataPath, "save.json");
    string json = JsonUtility.ToJosn(saveState);
  }
  public static void Load ()
  {
    string path = Path.Combine(Application.persistentDataPath, "save.json");
    if (!File.Exists(path)) return null;

    string json = File.ReadAllText(path);
    return JsonUtility.FromJson<SaveState>(json);
  }*/
}
