﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public string startScene = "Level 2";
    public string creidtScene = "Credits";

    public void NewGame()
    {
        SceneManager.LoadScene(startScene);
    }

    public void Credits()
    {
        SceneManager.LoadScene(creidtScene);
    }

    void Update()
   {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
   }
/*
    public void Continue ()
    {
        SavePointController.SaveState saveState = SavePointController.Load();
        if (saveState == null) NewGame();
        else GameManager.LoadSaveFile(saveState);
    }*/

}
