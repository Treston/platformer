﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemyHealthController : MonoBehaviour
{
    public delegate void HealthChanged(float health, float maxHealth);
    public event HealthChanged onEnemyHealthChanged = delegate { };

    public float enemyMaxHealth = 200;

    private float enemyHealth;

    void Start()
    {
        enemyHealth = enemyMaxHealth;
        onEnemyHealthChanged(enemyHealth, enemyMaxHealth);
    }

    public void EnemyChangeHealth(float change)
    {
        enemyHealth += change;
        onEnemyHealthChanged(enemyHealth, enemyMaxHealth);

        if (enemyHealth <= 0)
        {
            BossController.boss.Die();
        }
    }
}
