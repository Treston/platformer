﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class AutoBoxCollider : MonoBehaviour
{

#if UNITY_EDITOR

    public float edgeInset = 0.1f;
    BoxCollider2D box;
    SpriteRenderer sprite;
    void Start()
    {
        box = GetComponent<BoxCollider2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (Application.isPlaying) return;

        box.size = (Vector2)sprite.size - Vector2.one * edgeInset;
    }
#endif
}
