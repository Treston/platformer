﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditController : MonoBehaviour
{
   void Update()
   {
        bool reset = Input.GetKeyDown(KeyCode.Escape);

        if (reset == true)
        {
            SceneManager.LoadScene("Main Menu");
        }
   }
}