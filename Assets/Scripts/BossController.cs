﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossController : MonoBehaviour
{
    public float speed = 3;
    public static BossController boss;
    Rigidbody2D body;
    Animator anim;

    Vector2 targetVelocity;

    float _xInput = 0;
    public float xInput
    {
        get { return _xInput; }
        set { _xInput = value; }
    }
    
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        Vector3 dir = PlayerCOntroller.player.transform.position - transform.position;
        targetVelocity = body.velocity;
        targetVelocity.x = xInput * speed;
        anim.SetFloat("speed", Mathf.Abs(targetVelocity.x));
    }

    public void Die()
    {
        StartCoroutine(DieCoroutine());
    }

    IEnumerator DieCoroutine()
    {
        anim.SetTrigger("die");
        yield return new WaitForEndOfFrame();
        gameObject.SetActive(false);
        SceneManager.LoadScene("Credits");
    }
}
