﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float speed = 3;

    Rigidbody2D body;
    Animator anim;

    Vector2 targetVelocity;

    float _xInput = 0;
    public float xInput
    {
        get { return _xInput; }
        set { _xInput = value; }
    }
    
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        Vector3 dir = PlayerCOntroller.player.transform.position - transform.position;
        targetVelocity = body.velocity;
        targetVelocity.x = xInput * speed;
        anim.SetFloat("speed", Mathf.Abs(targetVelocity.x));
    }

    void Die()
    { 
        anim.SetTrigger("die");
        Vector3 dir = transform.position;
        gameObject.SetActive(false); 
    }
}
